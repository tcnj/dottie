# Dottie, the simple dotfiles manager

Dottie is a simple manager for your dotfiles (files in your user's home such as `.bashrc` or `.config/i3/config`). It allows you to store all your dotfiles in a single "base" directory (by default at `~/.dotfiles`) which will normally be in version control (eg. git). Dottie then automatically soft-links these files into their appropriate locations.

## Overlays

Within your base directory (which can be specified by either setting `$DOTTIE_BASE` or using a flag), dotfiles are organised into related groups called overlays. Each overlay is represented by a named folder within the base directory. When an overlay is engaged (touched on later), all the files within the overlay will be individually linked to a matching location within the target directory (by default your user's home, but overridable with `$DOTTIE_TARGET` or a flag). Any intermediate folders will be created automatically.

For instance, if the overlay `bash` is engaged, and there is a file at `$DOTTIE_BASE/bash/.bashrc`, then a link to it will be created at `$DOTTIE_TARGET/.bashrc`. If the overlay `i3` is engaged, and there is a file at `$DOTTIE_BASE/i3/.config/i3/config`, then a link to it will be created at `$DOTTIE_TARGET/.config/i3/config`, *unless* a file exists called `$DOTTIE_BASE/i3/.config/i3/.dottie_no_recurse`, in which case a link `$DOTTIE_BASE/i3/.config/i3` it will be created at `$DOTTIE_TARGET/.config/i3`.

### Engaging and disengaging overlays

Engaging and disengaging overlays is extremely simple, with one command corresponding to each. For instance:

```sh
dottie use bash
```

will engage the overlay named `bash`, whilst:

```sh
dottie unuse bash
```

will disengage the same overlay.

The subcommands `use` and `unuse` have the aliases `engage` and `disengage` respectively.

The subcommand `use` will always try to bring the target directory to where it should be if the overlay has been modified. That is, it will create any additional links even if some already exit. It is unfortunately not possible to remove dead links to non-existing files within the overlay, as that would require recursing the whole target directory, which would be inefficient for most home directories. If no new links are created, the subcommand will report that no changes have been made.

## Roles

Roles are a more advanced feature of dottie that allow you to easily engage or disengage groups of overlays at once, as well as ensure that a group of overlays are the only overlays engaged (or disengaged).

Each file under `$DOTTIE_BASE/roles/` corresponds to a role with the file's name. The format of a role file is extremely simple. Each line can be either blank, contain a comment beginning with `#`, or contain an overlay name. Comments are also allowed at the end of lines with overlay names. Whitespace is generally ignored.

The `roles` subcommand (which has the alias `role`) can be used for all manipulation of roles.

Files external to `$DOTTIE_BASE/roles/` can also be used as role files (with identical syntax). These can be specified with the `--file` (or `-f`) flag, in addition to or instead of named roles.

```sh
# Engages all overlays in the "workstation" and "ubuntu" roles
dottie roles workstation ubuntu

# Same as previous command, but disengages overlays
dottie roles --invert workstation ubuntu

# Engages all overlays in the "workstation" and "ubuntu" roles, and disengages any other overlays
dottie roles --exclusive workstation ubuntu

# Disengages all overlays in the "workstation" and "ubuntu" roles, and engages any other overlays (if you for some reason wanted to do this)
dottie roles --exclusive --invert workstation ubuntu

# Engages the role in the file /tmp/foo_role
dottie role --file /tmp/foo_role
```

All possible combinations of flags are valid, as long as they result in at least one role being specified. The flags `--exclusive` and `--invert` have short versions, being `-e` and `-i` respectively.

The one last feature of roles is the `all` meta-role. This role cannot be modified (a file in `$DOTTIE_BASE/roles/all` will be ignored unless specified with `--file` manually), and always contains all overlays found in `$DOTTIE_BASE`. This is useful for disengaging all overlays, or for engaging all overlays if your base directory structure is simple.

## Downloads

|Linux|
|-|
|[x86_64](https://gitlab.com/tcnj/dottie/-/jobs/artifacts/master/raw/target/x86_64-unknown-linux-musl/release/dottie?job=build)|
