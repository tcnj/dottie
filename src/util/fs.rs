use std::path::PathBuf;
use std::io;
use std::fs;

pub fn if_exists(path: PathBuf) -> Option<PathBuf> {
    if path.exists() {
        Some(path)
    }else{
        None
    }
}

pub fn if_directory_exists(path: PathBuf) -> Option<PathBuf> {
    if_exists(path)
        .and_then(|path| if path.is_dir() {
            Some(path)
        }else{
            None
        })
}

pub fn recurse_files(directory: &PathBuf) -> io::Result<Vec<PathBuf>> {
    if directory.is_dir() && !directory.join(".dottie_no_recurse").exists() {
        fs::read_dir(directory)
            .and_then(|files|
                files.map(|maybe_file|
                    maybe_file.and_then(|file| recurse_files(&file.path()))
                )
                .collect::<io::Result<Vec<Vec<PathBuf>>>>()
                .map(|vec| vec.concat())
            )
    }else{
        Ok(vec![directory.clone()])
    }
}

pub fn io_result<T>(result: io::Result<T>) -> Result<T, String> {
    result.map_err(|err| {format!("IO Error: {}", err)})
}