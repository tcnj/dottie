use std::fmt;

pub mod fs;

pub type CommandResult = Result<IdempotentOutcome<Vec<String>>, String>;
pub type IdempotentResult = Result<IdempotentOutcome<String>, String>;

pub enum IdempotentOutcome<T> {
    Unchanged(T),
    Changed(T)
}

use IdempotentOutcome::*;

impl<T> From<IdempotentOutcome<T>> for IdempotentOutcome<Vec<T>> {
    fn from(single: IdempotentOutcome<T>) -> IdempotentOutcome<Vec<T>> {
        match single {
            Unchanged(item) => Unchanged(vec![item]),
            Changed(item) => Changed(vec![item])
        }
    }
}

impl<T> IdempotentOutcome<Vec<T>> {
    pub fn merge_in(&mut self, other: IdempotentOutcome<T>) {
        match self {
            Unchanged(vec) => match other {
                Unchanged(item) => vec.push(item),
                Changed(item) => *self = Changed(vec![item])
            },
            Changed(vec) => match other {
                Unchanged(_item) => (),
                Changed(item) => vec.push(item)
            }
        }
    }

    pub fn merge_in_all(&mut self, other: IdempotentOutcome<Vec<T>>) {
        match self {
            Unchanged(vec) => match other {
                Unchanged(mut items) => vec.append(&mut items),
                Changed(items) => *self = Changed(items)
            },
            Changed(vec) => match other {
                Unchanged(_items) => (),
                Changed(mut items) => vec.append(&mut items)
            }
        }
    }

    pub fn new() -> IdempotentOutcome<Vec<T>> {
        Unchanged(Vec::new())
    }
}

impl<T> fmt::Display for IdempotentOutcome<Vec<T>> where T: fmt::Display {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Unchanged(vec) => match vec.last() {
                None => write!(f, "(Unchanged)"),
                Some(msg) => write!(f, "(Unchanged) {}", msg)
            },
            Changed(vec) => match vec.last() {
                None => write!(f, "(Changed)"),
                Some(msg) => write!(f, "(Changed) {}", msg)
            }
        }
    }
}

impl<T> fmt::Debug for IdempotentOutcome<Vec<T>> where T: fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let sep = if f.alternate() { "" } else { "\n" };
        match self {
            Unchanged(vec) =>
                vec.iter()
                    .map(|item| write!(f, "(Unchanged) {:?}{}", item, sep))
                    .collect::<Result<Vec<_>,_>>()
                    .map(|_| ()),
            Changed(vec) =>
                vec.iter()
                    .map(|item| write!(f, "(Changed) {:?}{}", item, sep))
                    .collect::<Result<Vec<_>,_>>()
                    .map(|_| ())
        }
    }
}