use std::env;
use std::path::PathBuf;

use clap::clap_app;

use dottie::util::fs as ufs;

fn main() {
    let mut clap_app = clap_app!(dottie =>
        (version: "0.1.0")
        (author: "(c) Danny Wensley 2020")
        (about: "A simple but powerful dotfile manager")
        (@arg base: -b --base +takes_value "Sets the base dotfile directory")
        (@arg target: -t --target +takes_value "Sets the target directory")
        (@subcommand use =>
            (alias: "engage")
            (about: "Engages a specific overlay")
            (@arg name: +required "Name of the overlay")
        )
        (@subcommand unuse =>
            (alias: "disengage")
            (about: "Disengages a specific overlay")
            (@arg name: +required "Name of overlay")
        )
        (@subcommand roles =>
            (alias: "role")
            (about: "Engages (or disengages) one or more roles")
            (@arg names: ... "Name(s) of role(s) to engage (or disengage)")
            (@arg files: -f --file +takes_value ... "Role file(s) to engage (or disengage) that live outside of the base dotfile directory")
            (@arg invert: -i --invert "Causes named role(s) to be disengaged rather than engaged")
            (@arg exclusive: -e --exclusive "Ensures that the listed roles are the only roles engaged (or disengaged), all overlays not included in the list will be disabled")
        )
    );

    let matches = clap_app.clone().get_matches();

    let dottie_base = matches.value_of("base")
        .map(PathBuf::from)
        .and_then(ufs::if_directory_exists)
        .or_else(|| env::var("DOTTIE_BASE").ok().map(PathBuf::from).and_then(ufs::if_directory_exists))
        .or_else(|| dirs::home_dir().map(|home| home.join(".dotfiles")).and_then(ufs::if_directory_exists));
    
    let target = matches.value_of("target")
        .map(PathBuf::from)
        .and_then(ufs::if_directory_exists)
        .or_else(|| env::var("DOTTIE_TARGET").ok().map(PathBuf::from).and_then(ufs::if_directory_exists))
        .or_else(|| dirs::home_dir().and_then(ufs::if_directory_exists));
    
    let dottie_base = unwrap_path_or_error(dottie_base, "base", "b", "base", ".dotfiles in your user directory", -1);
    let target = unwrap_path_or_error(target, "target", "t", "target", "your user directory", -2);

    match matches.subcommand() {
        ("use", Some(matches)) => dottie::use_(dottie_base, target, matches),
        ("unuse", Some(matches)) => dottie::unuse(dottie_base, target, matches),
        ("roles", Some(matches)) => {
            match dottie::roles(dottie_base, target, matches) {
                Ok(_) => (),
                Err(error) => eprintln!("{}", error)
            }
        },
        _ => {
            eprintln!("Error: expecting subcommand\n");
            clap_app.print_help().expect("Error printing help");
            println!();
        }
    }
}

fn unwrap_path_or_error(option: Option<PathBuf>, type_: &str, short_flag: &str, long_flag: &str, default: &str, exit: i32) -> PathBuf {
    match option {
        Some(v) => v,
        None => {
            eprintln!("Unable to find a {} directory. This can be remedied by making sure a path either specified in the environment variable DOTTIE_{}, given by the flag -{} or --{}, or {} exists.", type_, type_.to_uppercase(), short_flag, long_flag, default);
            std::process::exit(exit)
        }
    }
}
