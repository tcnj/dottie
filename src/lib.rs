use std::path::PathBuf;
use std::fs;

use clap::ArgMatches;

use crate::util::fs as ufs;
use crate::util::{IdempotentOutcome, IdempotentOutcome::*};

pub mod util;
mod overlay;

pub fn use_(dottie_base: PathBuf, target: PathBuf, matches: &ArgMatches) {
    let name = matches.value_of("name").unwrap();

    match overlay::use_(&dottie_base, &target, name) {
        Ok(msg) => println!("{}", msg),
        Err(msg) => eprintln!("{}", msg)
    }
}

pub fn unuse(dottie_base: PathBuf, target: PathBuf, matches: &ArgMatches) {
    let name = matches.value_of("name").unwrap();

    match overlay::unuse(&dottie_base, &target, name) {
        Ok(msg) => println!("{}", msg),
        Err(msg) => eprintln!("{}", msg)
    }
}

pub fn roles(dottie_base: PathBuf, target: PathBuf, matches: &ArgMatches) -> Result<(), String> {
    let is_all = matches.values_of("names")
        .and_then(|mut names|
            names.find(|name| *name == "all")
        )
        .is_some();
    
    let overlay_names = if is_all {
        overlay::list_overlays(&dottie_base)?
    }else{
        let mut role_name_paths: Vec<PathBuf> = matches.values_of("names")
            .map(|names|
                names.map(|role_name|
                    dottie_base.join("roles").join(role_name)
                )
                .collect()
            )
            .unwrap_or(Vec::new());
        
        let mut file_paths: Vec<PathBuf> = matches.values_of("files")
            .map(|files|
                files.map(|file|
                    PathBuf::from(file)
                )
                .collect()
            )
            .unwrap_or(Vec::new());
        
        let mut all_paths = Vec::new();

        all_paths.append(&mut role_name_paths);
        all_paths.append(&mut file_paths);

        if all_paths.len() == 0 {
            return Err("Expecting at least one role name of file path specified".to_string())
        }

        all_paths.iter()
            .map(|path|
                ufs::io_result(fs::read_to_string(path))
                    .map(|content|
                        content
                            .split('\n')
                            .filter_map(|line|
                                line.replace(' ', "")
                                    .replace('\n', "")
                                    .replace('\r', "")
                                    .split('#')
                                    .map(String::from)
                                    .next()
                            )
                            .filter_map(|line|
                                if line == "" { None } else { Some(line) }
                            )
                            .collect::<Vec<_>>()
                    )
            )
            .collect::<Result<Vec<_>, _>>()
            .map(|vec| vec.concat())?
    };
    
    let invert = matches.is_present("invert");
    let exclusive = matches.is_present("exclusive");

    let mut outcomes = IdempotentOutcome::new();

    for name in overlay_names.clone() {
        let result = if !invert {
            overlay::use_(&dottie_base, &target, name.as_ref())
        }else{
            overlay::unuse(&dottie_base, &target, name.as_ref())
        };

        outcomes.merge_in_all(result?);
    }

    if exclusive {
        let other_overlays = if !is_all {
            let mut other_overlays = overlay::list_overlays(&dottie_base)?;
            other_overlays.retain(|name| !overlay_names.contains(name));
            other_overlays
        }else{
            vec![]
        };

        for name in other_overlays {
            let result = if invert {
                overlay::use_(&dottie_base, &target, name.as_ref())
            }else{
                overlay::unuse(&dottie_base, &target, name.as_ref())
            };
    
            outcomes.merge_in_all(result?);
        }
    }

    let action = if invert {
        "disengaged"
    }else{
        "engaged"
    };

    outcomes.merge_in(match outcomes {
        Unchanged(_) => Unchanged(format!("Roles already {}", action)),
        Changed(_) => Changed(format!("Roles {}", action))
    });

    println!("{}", outcomes);

    return Ok(())
}