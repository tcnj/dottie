use std::path::{PathBuf, Path};
use std::fs;

use crate::util::{CommandResult, IdempotentResult, IdempotentOutcome::*, IdempotentOutcome};
use crate::util::fs as ufs;

const RESERVED_NAMES: [&'static str; 1] = ["roles"];

pub fn use_(dottie_base: &PathBuf, target: &PathBuf, name: &str) -> CommandResult {
    let overlay = get_overlay(dottie_base, name)?;

    let mut overlay_files = ufs::io_result(ufs::recurse_files(&overlay))?;
    
    let mut outcome = IdempotentOutcome::new();

    for file in overlay_files.iter_mut() {
        let suffix = get_suffix(&overlay, file)?;
        
        outcome.merge_in(link(&target.join(suffix), file)?);
    }

    outcome.merge_in(match outcome {
        Unchanged(_) => Unchanged(format!("Overlay {} already engaged", name)),
        Changed(_) => Changed(format!("Overlay {} engaged", name))
    });

    return Ok(outcome)
}

pub fn unuse(dottie_base: &PathBuf, target: &PathBuf, name: &str) -> CommandResult {
    let overlay = get_overlay(dottie_base, name)?;

    let mut overlay_files = ufs::io_result(ufs::recurse_files(&overlay))?;
    
    let mut outcome = IdempotentOutcome::new();

    for file in overlay_files.iter_mut() {
        let suffix = get_suffix(&overlay, file)?;
        
        outcome.merge_in(unlink(&target.join(suffix), file)?);
    }

    outcome.merge_in(match outcome {
        Unchanged(_) => Unchanged(format!("Overlay {} already disengaged", name)),
        Changed(_) => Changed(format!("Overlay {} disengaged", name))
    });

    return Ok(outcome)
}

pub fn list_overlays(dottie_base: &PathBuf) -> Result<Vec<String>, String> {
    let files = ufs::io_result(fs::read_dir(dottie_base))?;

    files.map(|maybe_file|
        ufs::io_result(maybe_file)
            .map(|dir_entry|
                if dir_entry.path().is_dir() && !RESERVED_NAMES.contains(&dir_entry.file_name().to_string_lossy().as_ref()) {
                    Some(dir_entry.file_name().to_string_lossy().to_string())
                }else{
                    None
                }
            )
    )
    .collect::<Result<Vec<_>,_>>()
    .map(|vec| vec.iter().cloned().filter_map(|name| name).collect())
}

fn get_overlay(dottie_base: &PathBuf, name: &str) -> Result<PathBuf, String> {
    if RESERVED_NAMES.contains(&name) {
        return Err(format!("An overlay named {} is not allowed, as the name is reserved.", name))
    }

    let overlay = dottie_base.join(name);

    if !overlay.is_dir() {
        return Err(format!("Overlay {} is a file, when it should be a directory.", name))
    }

    return Ok(overlay)
}

fn get_suffix<'a>(overlay: &PathBuf, file: &'a mut PathBuf) -> Result<&'a Path, String> {
    file.strip_prefix(overlay)
        .map_err(|_| format!(
            "File '{}' is not inside '{}'. This shouldn't normally happen, is some filesystem trickery going on?", 
            file.to_string_lossy(),
            overlay.to_string_lossy()
        ))
}

fn link(target: &PathBuf, source: &PathBuf) -> IdempotentResult {
    let source = ufs::io_result(fs::canonicalize(source.clone()))?;
    if target.exists() && ufs::io_result(fs::symlink_metadata(target))?.file_type().is_symlink() && ufs::io_result(fs::read_link(target))? == *source {
        Ok(Unchanged(format!("Path {} already links to {}", target.to_string_lossy(), source.to_string_lossy())))
    }else{
        if target.exists() {
            Err("Target exists but is not the correct link")?;
        }

        target.parent().map(|parent| ufs::io_result(fs::create_dir_all(parent))).transpose()?;

        std::os::unix::fs::symlink(&source, target)
            .map(|_| Changed(format!("Successfully linked {} to point at {}", target.to_string_lossy(), source.to_string_lossy())))
            .map_err(|err| format!("IO Error linking {} to point to {}: {}", target.to_string_lossy(), source.to_string_lossy(), err))
    }
}

fn unlink(target: &PathBuf, source: &PathBuf) -> IdempotentResult {
    let source = ufs::io_result(fs::canonicalize(source.clone()))?;
    if target.exists() {
        let metadata = ufs::io_result(fs::symlink_metadata(target))?;
        if metadata.file_type().is_symlink() {
            let links_source = ufs::io_result(fs::read_link(target))?;
            if links_source == *source {
                fs::remove_file(target)
                    .map(|_| Changed(format!("Successfully unlinked {} from pointing at {}", target.to_string_lossy(), source.to_string_lossy())))
                    .map_err(|err| format!("IO Error unlinking {} from pointing at {}: {}", target.to_string_lossy(), source.to_string_lossy(), err))
            }else{
                Err(format!("Target exists but is not the correct link. {} should link to {}, but instead it links to {}", target.to_string_lossy(), source.to_string_lossy(), links_source.to_string_lossy()))
            }
        }else{
            let file_type = if metadata.file_type().is_dir() {
                "a directory"
            }else if metadata.file_type().is_file() {
                "a file"
            }else{
                "an unknown file type"
            };

            Err(format!("Target exists but is not a link! {} should link to {}, but instead is {}", target.to_string_lossy(), source.to_string_lossy(), file_type))
        }
    }else{
        Ok(Unchanged(format!("Path {} is already unlinked", target.to_string_lossy())))
    }
}